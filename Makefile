.PHONY: gen_mocks lints lints_fix test coverage integration_test

gen_mocks:
	genmock -search.name=Storage -print.file.test -print.place.in_package -print.package.test

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml

test:
	go test -cover ./...

coverage:
	go test -coverpkg=./. -coverprofile=profile.cov ./.
	go tool cover -func profile.cov

	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'

integration_test:
	go test -v --tags=integration ./integration_test/...

