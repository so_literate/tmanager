package tmanager_test

import (
	"context"
	"encoding/json"
	"errors"
	"runtime"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/so_literate/tmanager"
)

func TestTaskManagerDefaults(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	tm := tmanager.New(nil, nil, 0, 0)

	so.Equal(runtime.NumCPU(), tm.WorkersNum)
	so.Equal(time.Second*10, tm.GetNextTaskInterval)
}

func TestTaskImpl(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	task := &tmanager.TaskImpl{
		ID:         "id",
		StartAfter: time.Now(),
		Handler:    "handler",
		Data:       []byte("{}"),
	}

	so.Equal(task.ID, task.GetID())
	so.Equal(task.StartAfter, task.GetStartAfter())
	so.Equal(task.Handler, task.GetHandler())
	so.Equal(task.Data, task.GetData())
}

var errTest = errors.New("errTest")

func TestCreateTask(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	s := NewStorage_Mock(t)
	s.ReturnMockErrorAsResult = true

	tm := tmanager.New(nil, s, 0, time.Millisecond*500)
	ctx, cancel := context.WithCancel(context.Background())

	task := &tmanager.TaskImpl{
		StartAfter: time.Now(),
		Handler:    "handler",
		Data:       []byte(`"data"`),
	}

	s.On_CreateTasks().Rets(nil)

	err := tm.CreateTask(ctx, task.StartAfter, task.Handler, "data")
	so.NoError(err)

	s.On_CreateTasks().Args(ctx, task).Rets(errTest)
	err = tm.CreateTask(ctx, task.StartAfter, task.Handler, "data")
	so.ErrorIs(err, errTest)

	err = tm.CreateTask(ctx, task.StartAfter, task.Handler, make(chan int))
	jErr := new(json.UnsupportedTypeError)
	so.ErrorAs(err, &jErr)

	cancel()

	s.On_CreateTasks().Args(ctx, task).Rets(context.Canceled)
	s.On_CreateTasks().Args(context.Background(), task).Rets(nil)
	err = tm.CreateTask(ctx, task.StartAfter, task.Handler, "data")
	so.NoError(err)

	err = s.Mock.AssertExpectations()
	so.NoError(err)
}
