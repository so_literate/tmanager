// +build integration

package integration_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/so_literate/tmanager"
	"gitlab.com/so_literate/tmanager/handler/cleaner"
	"gitlab.com/so_literate/tmanager/handler/inspector"
)

var (
	cleanDB = true
	errTest = errors.New("test error")
)

func TestStorageMethods(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	ctx := context.Background()
	h := newHelpers(t, "storage_methods")

	st := h.createStorage(cleanDB)

	task, err := st.GetNextTask(ctx, time.Now())
	so.Nil(task)
	so.ErrorIs(err, tmanager.ErrTaskNotFound)

	tasks := []tmanager.Task{
		&tmanager.TaskImpl{
			ID:         "1", // expected this sequence in database.
			StartAfter: time.Now(),
			Handler:    "test",
			Data:       nil,
		},
		&tmanager.TaskImpl{
			ID:         "2",
			StartAfter: time.Now(),
			Handler:    "test_2",
			Data:       []byte("{}"),
		},
	}

	err = st.CreateTasks(ctx, tasks...)
	so.NoError(err)

	h.checkRows(2, "")

	task1, err := st.GetNextTask(ctx, time.Now())
	so.NoError(err)
	so.NotNil(task1)
	h.compareTasks(tasks[0], task1)
	h.checkRows(1, "in_work_at IS NULL")

	task2, err := st.GetNextTask(ctx, time.Now())
	so.NoError(err)
	so.NotNil(task2)
	h.compareTasks(tasks[1], task2)
	h.checkRows(0, "in_work_at IS NULL")

	err = st.CreateTasks(ctx, &tmanager.TaskImpl{StartAfter: time.Now(), Handler: "test_3", Data: nil})
	so.NoError(err)
	h.checkRows(1, "in_work_at IS NULL")

	stuckedTasks, err := st.GetStuckedTasks(ctx, time.Now())
	so.NoError(err)
	so.Equal(2, len(stuckedTasks))
	h.compareTasks(tasks[0], stuckedTasks[0])
	h.compareTasks(tasks[1], stuckedTasks[1])

	err = st.SaveTaskWithSuccess(ctx, task1)
	so.NoError(err)
	h.checkRows(1, "done_at IS NOT NULL")

	err = st.SaveTaskWithError(ctx, task2, errTest)
	so.NoError(err)
	h.checkRows(1, "error IS NOT NULL")

	stuckedTasks, err = st.GetStuckedTasks(ctx, time.Now())
	so.NoError(err)
	so.Equal(0, len(stuckedTasks))

	failedTasks, err := st.GetFailedTasks(ctx)
	so.NoError(err)
	so.Equal(1, len(failedTasks))
	h.compareTasks(tasks[1], failedTasks[0])

	h.printRows()
	h.checkRows(3, "")

	err = st.DeleteNamedCompletedTasks(ctx, time.Now(), tasks[0].GetHandler())
	so.NoError(err)

	h.checkRows(2, "")

	err = st.DeleteCompletedTasks(ctx, time.Now())
	so.NoError(err)

	h.checkRows(2, "")
}

type ctxKey int

const (
	ctxKeyStartData ctxKey = iota + 1
	ctxKeyHandlerName
)

func TestTManagerBasic(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	ctx, cancel := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, ctxKeyStartData, 1)

	h := newHelpers(t, "tmanager_basic")
	st := h.createStorage(cleanDB)
	tm := tmanager.New(h, st, 0, time.Second)

	tm.BeforeHandler = func(ctx context.Context, task tmanager.Task) (context.Context, error) {
		ctx = context.WithValue(ctx, ctxKeyHandlerName, task.GetHandler())
		return ctx, nil
	}

	replacedContext := false
	tm.ReplaceCanceledContext = func(ctx context.Context) context.Context {
		replacedContext = true
		so.Equal(1, ctx.Value(ctxKeyStartData).(int))
		return context.Background()
	}

	m := newTaskMonitor(ctx, t, cancel, 4, 1)

	err := tm.RegisterHandlerFunc("withoutData", m.handler(func(ctx context.Context, data json.RawMessage) error {
		so.Equal("withoutData", ctx.Value(ctxKeyHandlerName).(string))
		so.Equal(json.RawMessage(nil), data)
		return nil
	}), nil)
	so.NoError(err)

	err = tm.RegisterHandlerFunc("withData", m.handler(func(ctx context.Context, data json.RawMessage) error {
		so.Equal("withData", ctx.Value(ctxKeyHandlerName).(string))
		so.Equal(json.RawMessage([]byte(`"data"`)), data)
		so.NoError(tm.CreateTask(ctx, time.Now(), "withError", nil))

		return nil
	}), nil)
	so.NoError(err)

	err = tm.RegisterHandlerFunc("withError", m.handler(func(ctx context.Context, data json.RawMessage) error {
		so.Equal("withError", ctx.Value(ctxKeyHandlerName).(string))
		so.Equal(json.RawMessage(nil), data)

		so.NoError(tm.CreateTask(ctx, time.Now(), "withoutData", nil))

		return errTest
	}), nil)
	so.NoError(err)

	so.NoError(tm.CreateTask(ctx, time.Now(), "withoutData", nil))
	so.NoError(tm.CreateTask(ctx, time.Now().Add(time.Second), "withData", "data"))
	so.NoError(tm.CreateTask(ctx, time.Now().Add(time.Second*2), "withoutData", nil))
	so.NoError(tm.CreateTask(ctx, time.Now().Add(time.Second), "withoutRoute", nil)) // return error before handler

	h.checkRows(4, "")

	go tm.Run(ctx)

	ok := m.runTimer()

	so.True(ok, "exit by timeout")
	so.True(replacedContext, "context must be replace before safe last task")

	h.checkRows(6, "")

	h.checkRows(4, "done_at IS NOT NULL")
	h.checkRows(2, "error IS NOT NULL")

	h.printRows()
	h.printLog()
}

func TestCreateOneActiveTask(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	ctx := context.Background()
	h := newHelpers(t, "one_active_task")

	st := h.createStorage(cleanDB)

	now := time.Now()

	task := &tmanager.TaskImpl{
		StartAfter: now.Add(-time.Second),
		Handler:    "first",
		Data:       nil,
	}

	err := st.CreateOneActiveTask(ctx, task)
	so.NoError(err)
	h.checkRows(1, "")

	err = st.CreateOneActiveTask(ctx, task)
	so.NoError(err)
	h.checkRows(1, "")

	err = st.CreateTasks(ctx, []tmanager.Task{task}...)
	so.Error(err)
	so.Truef(strings.Contains(err.Error(), "duplicate key value violates unique constraint"), err.Error())

	// Check duplicate tasks in statuses

	taskSt, err := st.GetNextTask(ctx, now)
	so.NoError(err)
	so.NotNil(taskSt)

	err = st.CreateOneActiveTask(ctx, task)
	so.NoError(err)
	h.checkRows(1, "")

	err = st.SaveTaskWithSuccess(ctx, taskSt)
	so.NoError(err)

	err = st.CreateOneActiveTask(ctx, task) // first task is done now you can create second task
	so.NoError(err)
	h.checkRows(2, "")

	err = st.CreateOneActiveTask(ctx, task)
	so.NoError(err)
	h.checkRows(2, "")

	taskSt, err = st.GetNextTask(ctx, now)
	so.NoError(err)
	so.NotNil(taskSt)

	err = st.SaveTaskWithError(ctx, taskSt, errTest)
	so.NoError(err)

	err = st.CreateOneActiveTask(ctx, task) // You can't to create a new task while you have undone task.
	so.NoError(err)
	h.checkRows(2, "")

	h.checkRows(0, "in_work_at IS NULL")
	err = st.RestartTask(ctx, taskSt, task.GetStartAfter())
	so.NoError(err)
	h.checkRows(1, "in_work_at IS NULL")

	taskSt, err = st.GetNextTask(ctx, now)
	so.NoError(err)
	so.NotNil(taskSt)

	h.checkRows(0, "in_work_at IS NULL")

	h.printRows()
}

func TestTManagerWithCallback(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	oneActiveTaskName := "one_active"
	oneActiveErrorTaskName := "one_active_with_error"

	ctx, cancel := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, ctxKeyStartData, 1)

	h := newHelpers(t, "tmanager_with_callback")
	st := h.createStorage(cleanDB)
	tm := tmanager.New(h, st, 2, time.Second)

	m := newTaskMonitor(ctx, t, cancel, 2, 2)

	callbackSuccess := func(ctx context.Context, task tmanager.Task, handlerErr error) {
		so.NoError(handlerErr)
		so.Equal(oneActiveTaskName, task.GetHandler())
		so.NoError(tm.CreateOneActiveTask(ctx, time.Now().Add(time.Second), oneActiveTaskName, nil))
	}

	err := tm.RegisterHandlerFunc(
		oneActiveTaskName,
		m.handler(func(ctx context.Context, data json.RawMessage) error { return nil }),
		callbackSuccess,
	)
	so.NoError(err)

	callbackError := func(ctx context.Context, task tmanager.Task, handlerErr error) {
		so.ErrorIs(handlerErr, errTest)
		so.Equal(oneActiveErrorTaskName, task.GetHandler())

		// you can't create new task when previous task done with error.
		so.NoError(tm.CreateOneActiveTask(ctx, time.Now(), oneActiveErrorTaskName, nil))

		// but you can restart your task
		so.NoError(tm.RestartTask(ctx, task, time.Now().Add(time.Second)))
	}

	err = tm.RegisterHandlerFunc(
		oneActiveErrorTaskName,
		m.handler(func(ctx context.Context, data json.RawMessage) error { return errTest }),
		callbackError,
	)
	so.NoError(err)

	so.NoError(tm.CreateTask(ctx, time.Now(), oneActiveTaskName, nil))
	so.NoError(tm.CreateTask(ctx, time.Now(), oneActiveErrorTaskName, nil))

	h.checkRows(2, "")

	go tm.Run(ctx)

	ok := m.runTimer()

	so.True(ok, "exit by timeout")

	h.printRows()
	h.checkRows(4, "")

	h.checkRows(2, "done_at IS NOT NULL")
	h.checkRows(1, "error IS NOT NULL")
	h.checkRows(2, "done_at IS NULL")
	h.checkRows(1, "done_at IS NULL AND error IS NOT NULL")
	h.checkRows(1, "done_at IS NULL AND error IS NULL")

	h.printLog()
}

func TestInspector(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	stuckedTaskName := "stucked_task"
	failedTaskName := "failed_task"

	ctx, cancel := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, ctxKeyStartData, 1)

	h := newHelpers(t, "inspector")
	st := h.createStorage(cleanDB)
	tm := tmanager.New(h, st, 3, time.Second)

	m := newTaskMonitor(ctx, t, cancel, 1, 1)

	err := tm.RegisterHandlerFunc(
		stuckedTaskName,
		m.handler(func(_ context.Context, _ json.RawMessage) error {
			time.Sleep(time.Second * 3)
			return nil
		}),
		nil,
	)
	so.NoError(err)

	err = tm.RegisterHandlerFunc(
		failedTaskName,
		m.handler(func(_ context.Context, _ json.RawMessage) error { return errTest }),
		nil,
	)
	so.NoError(err)

	so.NoError(tm.CreateTask(ctx, time.Now(), stuckedTaskName, nil))
	so.NoError(tm.CreateTask(ctx, time.Now(), failedTaskName, nil))

	onProblemTasksHandlerCalled := false

	onProblemTasksHandler := func(_ context.Context, stuckedTasks, failedTasks []inspector.Task) {
		so.Equal(1, len(stuckedTasks))
		so.Equal("1", stuckedTasks[0].GetID())

		so.Equal(1, len(failedTasks))
		so.Equal("2", failedTasks[0].GetID())
		taskErr := failedTasks[0].GetError()
		so.Truef(strings.Contains(taskErr, errTest.Error()), "error text wrong: %q", taskErr)

		onProblemTasksHandlerCalled = true
	}

	conf := &inspector.Config{
		StuckedInWorkAgo: time.Second,
		CheckTasksPeriod: time.Second * 2,
	}

	_, err = inspector.New(ctx, h, st, tm, onProblemTasksHandler, conf)
	so.NoError(err)

	go tm.Run(ctx)

	ok := m.runTimer()

	so.True(ok, "exit by timeout")

	h.printRows()
	h.checkRows(4, "")

	so.True(onProblemTasksHandlerCalled)

	h.checkRows(2, "done_at IS NOT NULL")
	h.checkRows(1, fmt.Sprintf("done_at IS NOT NULL AND handler != '%s'", stuckedTaskName))
	h.checkRows(1, "error IS NOT NULL")
	h.checkRows(1, "in_work_at IS NULL")

	h.printLog()
}

func TestCleaner(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	successTaskName := "success_task"
	failedTaskName := "failed_task"

	ctx, cancel := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, ctxKeyStartData, 1)

	h := newHelpers(t, "cleaner")
	st := h.createStorage(cleanDB)
	tm := tmanager.New(h, st, 3, time.Second)

	m := newTaskMonitor(ctx, t, cancel, 1, 1)

	err := tm.RegisterHandlerFunc(
		successTaskName,
		m.handler(func(_ context.Context, _ json.RawMessage) error {
			so.NoError(tm.CreateTask(ctx, time.Now().Add(time.Second*3), failedTaskName, nil))
			return nil
		}),
		nil,
	)
	so.NoError(err)

	err = tm.RegisterHandlerFunc(
		failedTaskName,
		m.handler(func(_ context.Context, _ json.RawMessage) error { return errTest }),
		nil,
	)
	so.NoError(err)

	so.NoError(tm.CreateTask(ctx, time.Now(), successTaskName, nil))

	conf := &cleaner.Config{
		OlderThen:        time.Millisecond * 500,
		CleanTasksPeriod: time.Second * 2,
	}

	_, err = cleaner.New(ctx, h, st, tm, conf)
	so.NoError(err)

	go tm.Run(ctx)

	ok := m.runTimer()

	so.True(ok, "exit by timeout")

	h.printRows()
	h.checkRows(3, "")
	h.checkRows(1, "done_at IS NOT NULL")
	h.checkRows(0, fmt.Sprintf("done_at IS NOT NULL AND handler != '%s'", cleaner.HandlerCleanerTask))
	h.checkRows(1, "error IS NOT NULL")

	h.printLog()
}

func TestCleanerNamedHandlers(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	successTaskName := "success_task"

	ctx, cancel := context.WithCancel(context.Background())
	ctx = context.WithValue(ctx, ctxKeyStartData, 1)

	h := newHelpers(t, "cleaner_named_handlers")
	st := h.createStorage(cleanDB)
	tm := tmanager.New(h, st, 2, time.Second)

	m := newTaskMonitor(ctx, t, cancel, 2, 0)

	err := tm.RegisterHandlerFunc(
		successTaskName,
		m.handler(func(_ context.Context, _ json.RawMessage) error {
			so.NoError(tm.CreateTask(ctx, time.Now().Add(time.Second*2), successTaskName, nil))
			return nil
		}),
		nil,
	)
	so.NoError(err)

	so.NoError(tm.CreateTask(ctx, time.Now().Add(time.Second), successTaskName, nil))

	conf := &cleaner.Config{
		OlderThen:        time.Millisecond,
		CleanTasksPeriod: time.Millisecond * 500,
		HandlerNames:     []string{cleaner.HandlerCleanerTask, "hanler_not_exists"},
	}

	_, err = cleaner.New(ctx, h, st, tm, conf)
	so.NoError(err)

	go tm.Run(ctx)

	ok := m.runTimer()

	so.True(ok, "exit by timeout")

	h.printRows()
	h.checkRows(5, "")
	h.checkRows(3, "done_at IS NOT NULL") // 2 success tasks and 1 cleaner
	h.checkRows(1, fmt.Sprintf("done_at IS NOT NULL AND handler = '%s'", cleaner.HandlerCleanerTask))
	h.checkRows(2, fmt.Sprintf("handler = '%s'", cleaner.HandlerCleanerTask))
	h.checkRows(0, "error IS NOT NULL")
	h.checkRows(2, fmt.Sprintf("done_at IS NOT NULL AND handler = '%s'", successTaskName))

	h.printLog()
}
