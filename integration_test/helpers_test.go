// +build integration

package integration_test

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync/atomic"
	"testing"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"
	"github.com/stretchr/testify/require"

	"gitlab.com/so_literate/tmanager"
	"gitlab.com/so_literate/tmanager/storage/postgresql"
)

type helpers struct {
	t  *testing.T
	so *require.Assertions

	pgConf *postgresql.Config
	conn   *sql.DB

	buf *bytes.Buffer
	log *log.Logger
}

func newHelpers(t *testing.T, schema string) *helpers {
	t.Helper()

	buf := bytes.NewBuffer([]byte("\n"))

	return &helpers{
		t:  t,
		so: require.New(t),

		pgConf: &postgresql.Config{
			Schema:    schema,
			TableName: "test_tasks",
		},

		buf: buf,
		log: log.New(buf, schema+": ", log.LstdFlags),
	}
}

func (h *helpers) createStorage(isCleanerEnable bool) *postgresql.Storage {
	h.t.Helper()

	dsn := os.Getenv("TMANAGER_DSN")
	if dsn == "" {
		dsn = "host=127.0.0.1 user=postgres password=postgres dbname=postgres port=5432 sslmode=disable"
	}

	conf, err := pgx.ParseConfig(dsn)
	h.so.NoErrorf(err, "pgx.ParseConfig")

	h.conn = stdlib.OpenDB(*conf)

	h.t.Cleanup(func() {
		if err = h.conn.Close(); err != nil {
			h.t.Logf("failed to close connect: %s", err)
		}
	})

	_, err = h.conn.Exec(fmt.Sprintf(`DROP SCHEMA IF EXISTS %s CASCADE;`, h.pgConf.Schema))
	h.so.NoErrorf(err, "failed to drop schema before create %q", h.pgConf.Schema)

	_, err = h.conn.Exec(fmt.Sprintf(`CREATE SCHEMA %s;`, h.pgConf.Schema))
	h.so.NoErrorf(err, "create test schema")

	if isCleanerEnable {
		h.t.Cleanup(func() {
			if _, err = h.conn.Exec(fmt.Sprintf(`DROP SCHEMA %s CASCADE;`, h.pgConf.Schema)); err != nil {
				h.t.Logf("failed to drop schema %q: %s", h.pgConf.Schema, err)
			}
		})
	}

	st, err := postgresql.New(h.conn, h.pgConf)
	h.so.NoErrorf(err, "postgresql.New")

	return st
}

func (h *helpers) checkRows(expectedRows int64, where string) {
	h.t.Helper()

	query := fmt.Sprintf(`SELECT count(1) FROM "%s"."%s" `, h.pgConf.Schema, h.pgConf.TableName)

	if where != "" {
		query += "WHERE " + where + ";"
	} else {
		query += ";"
	}

	var rows int64
	err := h.conn.QueryRow(query).Scan(&rows)

	h.so.NoError(err)
	h.so.Equalf(expectedRows, rows, "rows in database: where %q", where)
}

func (h *helpers) compareTasks(expectedTask, actualTask tmanager.Task) {
	h.t.Helper()

	h.so.Equalf(expectedTask.GetID(), actualTask.GetID(), "id")
	h.so.Equalf(expectedTask.GetHandler(), actualTask.GetHandler(), "handler")
	h.so.Equalf(
		expectedTask.GetStartAfter().Format(time.RFC3339),
		actualTask.GetStartAfter().Format(time.RFC3339),
		"start after",
	)
	h.so.Equalf(expectedTask.GetData(), actualTask.GetData(), "data")
}

func (h *helpers) printRows() {
	h.t.Helper()

	// nolint:rowserrcheck // this error is checked below
	rows, err := h.conn.Query(fmt.Sprintf(`SELECT * FROM "%s"."%s" ORDER BY id;`, h.pgConf.Schema, h.pgConf.TableName))
	h.so.NoError(err)

	defer func() {
		if errClose := rows.Close(); err != nil {
			h.t.Logf("failed to close printed rows: %s", errClose)
		}
	}()

	for rows.Next() {
		row := postgresql.Task{}

		err = rows.Scan(
			&row.ID, &row.CreatedAt, &row.UpdatedAt,
			&row.InWorkAt, &row.DoneAt, &row.Error,
			&row.StartAfter, &row.Handler, &row.Data,
		)
		h.so.NoError(err)

		h.t.Log(row.String())
	}

	h.so.NoError(rows.Err())
}

func (h *helpers) Write(p []byte) (int, error) {
	h.log.Printf("%s\n", p)
	return len(p), nil
}

func (h *helpers) printLog() {
	h.t.Log(h.buf.String())
}

type taskMonitor struct {
	t  *testing.T
	so *require.Assertions

	ctx    context.Context
	cancel context.CancelFunc

	doneTasks int64
	errTasks  int64
}

func newTaskMonitor(ctx context.Context, t *testing.T, cancel context.CancelFunc, doneTasks, errTasks int64) *taskMonitor {
	t.Helper()

	m := &taskMonitor{
		t:  t,
		so: require.New(t),

		ctx:    ctx,
		cancel: cancel,

		doneTasks: doneTasks,
		errTasks:  errTasks,
	}

	return m
}

func (m *taskMonitor) handler(h tmanager.HandlerFunc) tmanager.HandlerFunc {
	return func(ctx context.Context, data json.RawMessage) error {
		err := h(ctx, data)

		var done int64
		var errs int64

		if err == nil {
			done = atomic.AddInt64(&m.doneTasks, -1)
			errs = atomic.AddInt64(&m.errTasks, 0)
		} else {
			done = atomic.AddInt64(&m.doneTasks, 0)
			errs = atomic.AddInt64(&m.errTasks, -1)
		}

		switch {
		case done == 0 && errs == 0:
			m.cancel()
			m.t.Log("cancel context")

		case done < 0 || errs < 0:
			m.so.Failf("value < 0", "doneTasks == %d, errTasks == %d", done, errs)
		}

		return err
	}
}

func (m *taskMonitor) runTimer() bool {
	timeout := time.NewTimer(time.Second * 10)

	select {
	case <-m.ctx.Done():
		time.Sleep(time.Second)
		return true

	case <-timeout.C:
		m.t.Logf("done: %d, errs: %d", m.doneTasks, m.errTasks)
		m.cancel()
	}

	return false
}
