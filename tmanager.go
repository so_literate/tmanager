// Package tmanager contains methods for controlling the execution of tasks from the storage.
// Allows you to synchronize workflow execution.
package tmanager

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"runtime"
	"sync"
	"time"
)

var (
	// ErrTaskNotFound returns when storage can't find to free task.
	ErrTaskNotFound = errors.New("task not found")
	// ErrHandlerAlreadyExists returns in handler registration method.
	ErrHandlerAlreadyExists = errors.New("handler already exists")
	// ErrHandlerNotFound returns when task manager can't found handler for task.
	ErrHandlerNotFound = errors.New("handler not found")
	// ErrHandlerPanic returns when handler called panic.
	ErrHandlerPanic = errors.New("panic in the handler")
)

// Task is a element of the storage with handler name, launch time and additional data.
type Task interface {
	GetID() string            // Unique identification of the task.
	GetStartAfter() time.Time // Time after which the task should be started.
	GetHandler() string       // Name of the task handler.
	GetData() json.RawMessage // Data of the handler.
}

// TaskImpl implements task interface for helper function of task creation.
type TaskImpl struct {
	ID         string
	StartAfter time.Time
	Handler    string
	Data       json.RawMessage
}

// GetID returns unique identification of the task.
func (t *TaskImpl) GetID() string { return t.ID }

// GetStartAfter returns time after which the task should be started.
func (t *TaskImpl) GetStartAfter() time.Time { return t.StartAfter }

// GetHandler returns name of the task handler.
func (t *TaskImpl) GetHandler() string { return t.Handler }

// GetData returns data of the handler.
func (t *TaskImpl) GetData() json.RawMessage { return t.Data }

// Storage is a task storage for task manager.
// It contains CRU operations.
type Storage interface {
	// CreateTasks creates list of the tasks in the storage.
	// Task field ID must be empty.
	CreateTasks(ctx context.Context, tasks ...Task) error

	// CreateOneActiveTask checks that task already exists in storage.
	// If the task does not exist in the storage, a new task is created.
	CreateOneActiveTask(ctx context.Context, task Task) error

	// GetNextTask returns the oldest task that the worker has not yet taken to work.
	// Value of the StartAfter must be less then "now" argument.
	// Returns error ErrTaskNotFound when storage can't found task.
	GetNextTask(ctx context.Context, now time.Time) (Task, error)

	// RestartTask makes the task free to re-taking in GetNextTask method.
	// You have to set time to re-taking.
	RestartTask(ctx context.Context, task Task, restartAfter time.Time) error

	// SaveTaskWithSuccess saves success done task.
	// If the context is canceled, then ctx will be the default context.Background here
	// to be sure to save the task state. See ReplaceCanceledContext method in TaskManager.
	SaveTaskWithSuccess(ctx context.Context, task Task) error
	// SaveTaskWithError saves task with error.
	// If the context is canceled, then ctx will be the default context.Background here
	// to be sure to save the task state. See ReplaceCanceledContext method in TaskManager.
	SaveTaskWithError(ctx context.Context, task Task, taskErr error) error
}

type routerHandler struct {
	handler  HandlerFunc
	callback HandlerCallbackFunc
}

// TaskManager creates and handle tasks in multithread workers.
type TaskManager struct {
	Logger  *log.Logger // Writes errors of interaction with the Storage.
	Storage Storage

	WorkersNum          int
	GetNextTaskInterval time.Duration

	// BeforeHandler calls before each task handler.
	// You can use it to add your data to context or to stop handling task.
	// Example:
	//    tm.BeforeHandler = func(ctx context.Context, task *tmanager.Task) (context.Context, error) {
	//    return context.WithValue(ctx, "task", task), nil
	//  }
	BeforeHandler func(ctx context.Context, task Task) (context.Context, error)

	// ReplaceCanceledContext calls before SaveTaskWithSuccess, SaveTaskWithError and CreateTasks
	// methods if current context is canceled.
	// Raplaces current context with context.Background by default to remove cancel function from context.
	// This is necessary to be sure to save the state of tasks after handling them,
	// ignoring the cancellation of the context.
	// You can disable this feature by setting this method to nil.
	// Example:
	//  tm.ReplaceCanceledContext = func(ctx context.Context) context.Context {
	//    return context.WithValue(context.Background(), "old_data", ctx.Value("old_data"))
	//  }
	ReplaceCanceledContext func(ctx context.Context) context.Context

	mu     *sync.Mutex
	router map[string]*routerHandler
}

// New creates new task manager with settings.
// workersNum is a number of individual workers trying to get and handle tasks.
// getNextTaskInterval is waiting time to try to get next task, when storage not retuned a task.
func New(errLog io.Writer, s Storage, workersNum int, getNextTaskInterval time.Duration) *TaskManager {
	if workersNum == 0 {
		workersNum = runtime.NumCPU()
	}

	if getNextTaskInterval == 0 {
		getNextTaskInterval = time.Second * 10 // nolint:gomnd // each 10 second by default
	}

	return &TaskManager{
		Logger:  log.New(errLog, "[tmanager]: ", 0),
		Storage: s,

		WorkersNum:          workersNum,
		GetNextTaskInterval: getNextTaskInterval,

		ReplaceCanceledContext: func(ctx context.Context) context.Context {
			return context.Background()
		},

		mu:     new(sync.Mutex),
		router: make(map[string]*routerHandler, 4),
	}
}

// execInRepeat execute function with context and checks that context is canceled.
// If context is canceled then function will be repeat with replaced context.
func (tm *TaskManager) execInRepeat(ctx context.Context, exec func(context.Context) error) error {
	err := exec(ctx)
	if err == nil {
		return nil
	}

	if tm.ReplaceCanceledContext == nil { // we can't to repeat with old context
		return err
	}

	// check that it is error with canceled context

	select {
	case <-ctx.Done():
	default:
		return err
	}

	return exec(tm.ReplaceCanceledContext(ctx))
}

// HandlerFunc func handler of the task. It takes context and stored data of the task.
type HandlerFunc func(ctx context.Context, data json.RawMessage) error

// HandlerCallbackFunc is a callback called after saved state of the task.
// Takes context of the task manager and error from HandlerFunc.
type HandlerCallbackFunc func(ctx context.Context, task Task, handlerErr error)

// RegisterHandlerFunc adds a new task handler function to the manager.
// Takes name of the handler, handler and optional (may be nil) callback of the task.
func (tm *TaskManager) RegisterHandlerFunc(handlerName string, h HandlerFunc, cb HandlerCallbackFunc) error {
	tm.mu.Lock()
	defer tm.mu.Unlock()

	if _, ok := tm.router[handlerName]; ok {
		return fmt.Errorf("%w: handler name %q", ErrHandlerAlreadyExists, handlerName)
	}

	rh := &routerHandler{
		handler:  h,
		callback: cb,
	}

	tm.router[handlerName] = rh

	return nil
}

// Handler task handler interaface.
type Handler interface {
	// HandleTask handler of the task.
	HandleTask(ctx context.Context, data json.RawMessage) error
}

// RegisterHandler adds a new task handler to the manager.
func (tm *TaskManager) RegisterHandler(handlerName string, h Handler) error {
	return tm.RegisterHandlerFunc(handlerName, h.HandleTask, nil)
}

// HandlerCallback task handler with callback.
type HandlerCallback interface {
	Handler
	// CallbackTask called after saved task state.
	CallbackTask(ctx context.Context, task Task, handlerErr error)
}

// RegisterHandlerCallback adds a new task handler with callback to the manager.
func (tm *TaskManager) RegisterHandlerCallback(handlerName string, h HandlerCallback) error {
	return tm.RegisterHandlerFunc(handlerName, h.HandleTask, h.CallbackTask)
}

// CreateTasks creates one or list of the tasks in the storage.
func (tm *TaskManager) CreateTasks(ctx context.Context, tasks ...Task) error {
	return tm.execInRepeat(ctx, func(execCtx context.Context) error {
		if err := tm.Storage.CreateTasks(execCtx, tasks...); err != nil {
			return fmt.Errorf("tm.Storage.CreateTasks: %w", err)
		}

		return nil
	})
}

func buildTask(startAfter time.Time, handlerName string, data interface{}) (Task, error) {
	var jData json.RawMessage
	var err error

	if data != nil {
		jData, err = json.Marshal(data)
		if err != nil {
			return nil, fmt.Errorf("json.Marshal data: %w", err)
		}
	}

	task := &TaskImpl{
		StartAfter: startAfter,
		Handler:    handlerName,
		Data:       jData,
	}

	return task, nil
}

// CreateTask converts data to the JSON and creates task in the storage.
func (tm *TaskManager) CreateTask(
	ctx context.Context,
	startAfter time.Time,
	handlerName string,
	data interface{},
) error {
	task, err := buildTask(startAfter, handlerName, data)
	if err != nil {
		return fmt.Errorf("buildTask: %w", err)
	}

	if err = tm.CreateTasks(ctx, task); err != nil {
		return fmt.Errorf("tm.CreateTasks: %w", err)
	}

	return nil
}

// CreateOneActiveTask creates a task, checking that
// there are no other tasks in the store with the same handler.
// Do not use it for regular tasks.
func (tm *TaskManager) CreateOneActiveTask(
	ctx context.Context,
	startAfter time.Time,
	handlerName string,
	data interface{},
) error {
	task, err := buildTask(startAfter, handlerName, data)
	if err != nil {
		return fmt.Errorf("buildTask: %w", err)
	}

	return tm.execInRepeat(ctx, func(execCtx context.Context) error {
		if err := tm.Storage.CreateOneActiveTask(execCtx, task); err != nil {
			return fmt.Errorf("tm.Storage.CreateOneActiveTask: %w", err)
		}

		return nil
	})
}

// RestartTask makes the task free to re-taking.
func (tm *TaskManager) RestartTask(ctx context.Context, task Task, restartAfter time.Time) error {
	return tm.execInRepeat(ctx, func(execCtx context.Context) error {
		if err := tm.Storage.RestartTask(execCtx, task, restartAfter); err != nil {
			return fmt.Errorf("tm.Storage.RestartTask: %w", err)
		}

		return nil
	})
}

// Run runs workers of the manager.
// You have to cancel context if you want to stop manager and workers.
// Task Manager will wait for all workers to finish.
func (tm *TaskManager) Run(ctx context.Context) {
	wg := new(sync.WaitGroup)

	for i := 0; i < tm.WorkersNum; i++ {
		wg.Add(1)

		go func() {
			tm.runWorker(ctx)
			wg.Done()
		}()
	}

	wg.Wait()
}

func (tm *TaskManager) logError(err error) {
	tm.Logger.Printf("%s\n", err)
}

func (tm *TaskManager) handleTask(ctx context.Context, task Task) (cb HandlerCallbackFunc, err error) {
	tm.mu.Lock()
	rh, ok := tm.router[task.GetHandler()]
	tm.mu.Unlock()

	if !ok {
		return nil, fmt.Errorf("%w: handler %q", ErrHandlerNotFound, task.GetHandler())
	}

	cb = rh.callback

	if tm.BeforeHandler != nil {
		if ctx, err = tm.BeforeHandler(ctx, task); err != nil {
			return cb, fmt.Errorf("tm.BeforeHandler: %w", err)
		}
	}

	defer func() {
		if rec := recover(); rec != nil {
			err = fmt.Errorf("%w %q: %v", ErrHandlerPanic, task.GetHandler(), rec)
		}
	}()

	if err = rh.handler(ctx, task.GetData()); err != nil {
		return cb, fmt.Errorf("handler %q: %w", task.GetHandler(), err)
	}

	return cb, nil
}

func (tm *TaskManager) tryHandleTask(ctx context.Context) (doneTask bool) {
	task, err := tm.Storage.GetNextTask(ctx, time.Now())
	if err != nil {
		if errors.Is(err, ErrTaskNotFound) {
			return false
		}

		select {
		case <-ctx.Done():
			return false
		default:
		}

		tm.logError(fmt.Errorf("tm.Storage.GetNextTask: %w", err))
		return false
	}

	callback, err := tm.handleTask(ctx, task)

	defer func() {
		if callback != nil {
			callback(ctx, task, err)
		}
	}()

	if err != nil {
		saveErr := tm.execInRepeat(ctx, func(execCtx context.Context) error {
			return tm.Storage.SaveTaskWithError(execCtx, task, err) // nolint:wrapcheck // don't need context
		})

		if saveErr != nil {
			tm.logError(fmt.Errorf("tm.Storage.SaveTaskWithError (%s): %w, task error: %q",
				task.GetID(), saveErr, err))
		}

		return false // make pause before request a next task
	}

	saveErr := tm.execInRepeat(ctx, func(execCtx context.Context) error {
		return tm.Storage.SaveTaskWithSuccess(execCtx, task) // nolint:wrapcheck // don't need context
	})

	if saveErr != nil {
		tm.logError(fmt.Errorf("tm.Storage.SaveTaskWithSuccess (%s): %w", task.GetID(), saveErr))
	}

	return true
}

func (tm *TaskManager) runWorker(ctx context.Context) {
	timer := time.NewTimer(tm.GetNextTaskInterval)

	defer func() {
		if !timer.Stop() {
			<-timer.C
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return

		case <-timer.C:
			doneTask := tm.tryHandleTask(ctx)
			if doneTask {
				timer.Reset(0) // try to get next task right now if the worker done last task
			} else {
				timer.Reset(tm.GetNextTaskInterval)
			}
		}
	}
}
