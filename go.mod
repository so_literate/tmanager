module gitlab.com/so_literate/tmanager

go 1.16

require (
	github.com/jackc/pgtype v1.7.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/so_literate/genmock v0.0.5
)
