package main

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/stdlib"

	"gitlab.com/so_literate/tmanager"
	"gitlab.com/so_literate/tmanager/handler/cleaner"
	"gitlab.com/so_literate/tmanager/handler/inspector"
	"gitlab.com/so_literate/tmanager/storage/postgresql"
)

var (
	dsn = "host=127.0.0.1 user=postgres password=postgres dbname=postgres port=5432 sslmode=disable"

	selfRepeatedTask = "self_repeated_task"
)

type taskHandler struct {
	tm *tmanager.TaskManager
}

func (t *taskHandler) HandleTask(ctx context.Context, data json.RawMessage) error {
	log.Println("called a task handler")
	return nil
}

func (t *taskHandler) CallbackTask(ctx context.Context, task tmanager.Task, handlerErr error) {
	nextTime := time.Now().Add(time.Second * 5)

	if handlerErr != nil {
		log.Printf("error in handler: %s\n", handlerErr)
		err := t.tm.RestartTask(ctx, task, nextTime)
		if err != nil {
			log.Printf("failed to restart task: %s\n", err)
		}

		return
	}

	err := t.tm.CreateOneActiveTask(ctx, nextTime, task.GetHandler(), nil)
	if err != nil {
		log.Printf("failed to created task: %s\n", err)
	}
}

func main() {
	conf, err := pgx.ParseConfig(dsn)
	if err != nil {
		log.Fatalf("pgx.ParseConfig: %s", err)
	}

	db := stdlib.OpenDB(*conf)

	storage, err := postgresql.New(db, nil)
	if err != nil {
		log.Fatalf("postgresql.New: %s", err)
	}

	tm := tmanager.New(log.Writer(), storage, 0, time.Second*10)
	ctx := context.Background()

	err = tm.RegisterHandlerCallback(selfRepeatedTask, &taskHandler{tm: tm})
	if err != nil {
		log.Fatalf("tm.RegisterHandlerCallback: %s\n", err)
	}

	// CreateOneActiveTask method creates database duplicate control index.
	err = tm.CreateOneActiveTask(ctx, time.Now(), selfRepeatedTask, nil)
	if err != nil {
		log.Fatalf("tm.CreateOneActiveTask selfRepeatedTask: %s\n", err)
	}

	cleanerCfg := &cleaner.Config{ // Cleaner will clean only technical old tasks.
		HandlerNames: []string{cleaner.HandlerCleanerTask, inspector.HandlerInspectorTask},
	}

	_, err = cleaner.New(ctx, log.Writer(), storage, tm, cleanerCfg)
	if err != nil {
		log.Fatalf("cleaner.New: %s\n", err)
	}

	// logInsp have callbacks with logger on failed and stucked tasks
	logInsp := inspector.NewLoggerInspector(log.Writer())

	_, err = inspector.New(ctx, log.Writer(), storage, tm, logInsp.OnInspectedTasks, nil)
	if err != nil {
		log.Fatalf("inspector.New: %s\n", err)
	}

	log.Println("run task manager")

	tm.Run(ctx)
}
