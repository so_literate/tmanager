// Package inspector contains handler to inspect stucked tasks in storage.
package inspector

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"time"

	"gitlab.com/so_literate/tmanager"
)

// HandlerInspectorTask names of the inspector handler in the task manager.
var HandlerInspectorTask = "_inspector"

// Config configuration of the inspector tasks.
type Config struct {
	// StuckedInWorkAgo task will be considered stucked if
	// it was taken to work more than an StuckedInWorkAgo ago.
	StuckedInWorkAgo time.Duration

	// CheckTasksPeriod inspector of the problem tasks
	// will run every specified time period.
	CheckTasksPeriod time.Duration
}

// Task is the task type extension to get the error text from task.
type Task interface {
	tmanager.Task

	// GetError returns text of the task stored in the Storage.
	GetError() string
}

// Storage of the task with stucked and failed tasks.
type Storage interface {
	// GetStuckedTasks returns stucked tasks.
	// Stuck task it is task without error and taked in work before inWorkBefore.
	// Returns empty slcie when storage can't found stucked tasks.
	GetStuckedTasks(ctx context.Context, inWorkBefore time.Time) ([]Task, error)

	// GetFailedTasks returns failed tasks.
	// Failed task it is undone task with error.
	// Returns empty slcie when storage can't found failed tasks.
	GetFailedTasks(ctx context.Context) ([]Task, error)
}

// OnInspectedTasks user actions on inspected tasks.
// Takes task manager context and lists of the stucked and failed tasks.
type OnInspectedTasks func(ctx context.Context, stuckedTasks, failedTasks []Task)

// Inspector contains task manager handlers to check stucked and failed tasks.
type Inspector struct {
	Config  *Config
	Logger  *log.Logger
	storage Storage
	tm      *tmanager.TaskManager

	OnInspectedTasks OnInspectedTasks
}

func checkAndSetDefaultConfigValues(cfg *Config) {
	if cfg.StuckedInWorkAgo == 0 {
		cfg.StuckedInWorkAgo = time.Minute * 20 // nolint:gomnd // tasks taked in work 20 minutes ago
	}

	if cfg.CheckTasksPeriod == 0 {
		cfg.CheckTasksPeriod = time.Minute * 5 // nolint:gomnd // check tasks each 5 minutes
	}
}

// New creates new Inspector. Takes storage and user handlers on inspected task.
// Feel free to pass nil in config.
func New(
	ctx context.Context,
	errLog io.Writer,
	st Storage,
	tm *tmanager.TaskManager,
	onInspectedTasks OnInspectedTasks,
	config *Config,
) (*Inspector, error) {
	if config == nil {
		config = new(Config)
	}

	checkAndSetDefaultConfigValues(config)

	insp := &Inspector{
		Config:  config,
		Logger:  log.New(errLog, "[tmanager.inspector]: ", 0),
		storage: st,
		tm:      tm,

		OnInspectedTasks: onInspectedTasks,
	}

	if err := insp.createTaskInspector(ctx); err != nil {
		return nil, fmt.Errorf("createTaskInspector: %w", err)
	}

	return insp, nil
}

func (insp *Inspector) createTaskInspector(ctx context.Context) error {
	err := insp.tm.RegisterHandlerCallback(HandlerInspectorTask, insp)
	if err != nil {
		return fmt.Errorf("tm.RegisterHandlerFunc: %w", err)
	}

	startAfter := time.Now().Add(insp.Config.CheckTasksPeriod)

	err = insp.tm.CreateOneActiveTask(ctx, startAfter, HandlerInspectorTask, nil)
	if err != nil {
		return fmt.Errorf("storage.CreateOneActiveTask: %w", err)
	}

	return nil
}

func (insp *Inspector) restartTask(ctx context.Context, task tmanager.Task, reStartAfter time.Time, isRepeat bool) {
	err := insp.tm.RestartTask(ctx, task, reStartAfter)
	if err == nil {
		return
	}

	if !isRepeat { // try to restart task after second
		time.Sleep(time.Second)
		insp.restartTask(ctx, task, reStartAfter, true)
		return
	}

	insp.Logger.Printf("failed to restart task %q: %s", task.GetID(), err)
}

func (insp *Inspector) createTask(ctx context.Context, startAfter time.Time, isRepeat bool) {
	err := insp.tm.CreateOneActiveTask(ctx, startAfter, HandlerInspectorTask, nil)
	if err == nil {
		return
	}

	if !isRepeat { // try to create task after second
		time.Sleep(time.Second)
		insp.createTask(ctx, startAfter, true)
		return
	}

	insp.Logger.Printf("failed to create task: %s\n", err)
}

// HandleTask calls storage method to clean completed task older then config param.
func (insp *Inspector) HandleTask(ctx context.Context, _ json.RawMessage) error {
	stuckedTasks, err := insp.storage.GetStuckedTasks(ctx, time.Now().Add(-insp.Config.StuckedInWorkAgo))
	if err != nil {
		return fmt.Errorf("storage.GetStuckedTasks: %w", err)
	}

	failedTasks, err := insp.storage.GetFailedTasks(ctx)
	if err != nil {
		return fmt.Errorf("storage.GetFailedTasks: %w", err)
	}

	insp.OnInspectedTasks(ctx, stuckedTasks, failedTasks)

	return nil
}

// CallbackTask self repeated callback.
func (insp *Inspector) CallbackTask(ctx context.Context, task tmanager.Task, handlerErr error) {
	nextTime := time.Now().Add(insp.Config.CheckTasksPeriod)

	if handlerErr != nil {
		insp.Logger.Printf("error in handler: %s\n", handlerErr)
		insp.restartTask(ctx, task, nextTime, false)
		return
	}

	insp.createTask(ctx, nextTime, false)
}
