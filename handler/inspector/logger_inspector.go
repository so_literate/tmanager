package inspector

import (
	"context"
	"io"
	"log"
)

// LoggerInspector inspector with logger just write about stucked and failed tasks.
type LoggerInspector struct {
	Logger *log.Logger
}

// NewLoggerInspector returns new inspector with logger.
//  li := inspector.NewLoggerInspector(log)
//  insp, err := inspector.New(..., li.OnInspectedTasks, nil)
func NewLoggerInspector(logWriter io.Writer) *LoggerInspector {
	return &LoggerInspector{
		Logger: log.New(logWriter, "[logger-inspector]: ", 0),
	}
}

func (li *LoggerInspector) logBadTask(count int, taskType string) {
	li.Logger.Printf("%d %s tasks in storage\n", count, taskType)
}

// OnInspectedTasks inspector callback prints count of the stucked and failed tasks.
func (li *LoggerInspector) OnInspectedTasks(_ context.Context, stuckedTasks, failedTasks []Task) {
	if len(stuckedTasks) != 0 {
		li.logBadTask(len(stuckedTasks), "stucked")
	}

	if len(failedTasks) != 0 {
		li.logBadTask(len(failedTasks), "failed")
	}
}
