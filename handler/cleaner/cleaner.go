// Package cleaner contains handler to clean completed task N time ago.
package cleaner

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"time"

	"gitlab.com/so_literate/tmanager"
)

// HandlerCleanerTask name of the cleaner task name.
var HandlerCleanerTask = "_cleaner"

// Config configuration of the cleaner tasks.
type Config struct {
	// OlderThen task should be older than this parameter to be deleted.
	OlderThen time.Duration

	// CleanTasksPeriod cleaner of the old completed tasks will run every specified time period.
	CleanTasksPeriod time.Duration

	// HandlerNames names of the handler to be deleted.
	// If empty then cleaner will delete all old tasks.
	HandlerNames []string
}

// Storage of the task with old tasks.
type Storage interface {
	// DeleteCompletedTasks delete completed tasks older then olderThen param.
	DeleteCompletedTasks(ctx context.Context, olderThen time.Time) error

	// DeleteNamedCompletedTasks delete completed tasks with special handler names
	// and completed before olderThen param.
	DeleteNamedCompletedTasks(ctx context.Context, olderThen time.Time, names ...string) error
}

// Cleaner contains clean handler and self repeated callback.
type Cleaner struct {
	Config  *Config
	Logger  *log.Logger
	storage Storage
	tm      *tmanager.TaskManager
}

func checkAndSetDefaultConfigValues(cfg *Config) {
	if cfg.OlderThen == 0 {
		cfg.OlderThen = time.Hour * 24 * 7 * 4 // nolint:gomnd // deleted completed tasks older then 4 weeks.
	}

	if cfg.CleanTasksPeriod == 0 {
		cfg.CleanTasksPeriod = time.Hour * 24 // nolint:gomnd // cleaner will be run every day.
	}
}

// New creates new cleaner task in the task manager and returns Cleaner with logger and config.
// Feel free to pass nil in config.
func New(
	ctx context.Context,
	errLog io.Writer,
	st Storage,
	tm *tmanager.TaskManager,
	config *Config,
) (*Cleaner, error) {
	if config == nil {
		config = new(Config)
	}
	checkAndSetDefaultConfigValues(config)

	c := &Cleaner{
		Config:  config,
		Logger:  log.New(errLog, "[tmanager.cleaner]: ", 0),
		storage: st,
		tm:      tm,
	}

	if err := c.createTask(ctx); err != nil {
		return nil, fmt.Errorf("createTask: %w", err)
	}

	return c, nil
}

func (c *Cleaner) createTask(ctx context.Context) error {
	err := c.tm.RegisterHandlerCallback(HandlerCleanerTask, c)
	if err != nil {
		return fmt.Errorf("tm.RegisterHandlerCallback: %w", err)
	}

	startAfter := time.Now().Add(c.Config.CleanTasksPeriod)

	err = c.tm.CreateOneActiveTask(ctx, startAfter, HandlerCleanerTask, nil)
	if err != nil {
		return fmt.Errorf("tm.CreateOneActiveTask: %w", err)
	}

	return nil
}

// HandleTask calls storage method to clean completed task older then config param.
func (c *Cleaner) HandleTask(ctx context.Context, _ json.RawMessage) error {
	var err error
	olderThen := time.Now().Add(-c.Config.OlderThen)

	if len(c.Config.HandlerNames) == 0 { // delete all older tasks
		err = c.storage.DeleteCompletedTasks(ctx, olderThen)
	} else {
		err = c.storage.DeleteNamedCompletedTasks(ctx, olderThen, c.Config.HandlerNames...)
	}

	if err != nil {
		return fmt.Errorf("delete completed tasks: %w", err)
	}

	return nil
}

// CallbackTask self repeated callback.
func (c *Cleaner) CallbackTask(ctx context.Context, task tmanager.Task, handlerErr error) {
	if handlerErr != nil {
		c.Logger.Printf("error in cleaner handler: %s\n", handlerErr)

		restartTime := time.Now().Add(time.Minute) // restart after 1 minute.

		if err := c.tm.RestartTask(ctx, task, restartTime); err != nil {
			c.Logger.Printf("failed to restart cleaner task: %s\n", err)
		}

		return
	}

	startAfter := time.Now().Add(c.Config.CleanTasksPeriod)

	err := c.tm.CreateOneActiveTask(ctx, startAfter, HandlerCleanerTask, nil)
	if err != nil {
		c.Logger.Printf("failed to create cleaner task: %s\n", err)
	}
}
