package postgresql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgtype"

	"gitlab.com/so_literate/tmanager"
	"gitlab.com/so_literate/tmanager/handler/inspector"
)

func rollback(tx *sql.Tx, originErr error) error {
	rollbackErr := tx.Rollback()
	if rollbackErr != nil {
		return fmt.Errorf("%w (tx.Rollback: %s)", originErr, rollbackErr)
	}

	return originErr
}

func (s *Storage) getCurrentSchema(ctx context.Context) (string, error) {
	var schema string
	if err := s.conn.QueryRowContext(ctx, `SELECT current_schema()`).Scan(&schema); err != nil {
		return "", fmt.Errorf("conn.QueryRow: %w", err)
	}

	return schema, nil
}

func (s *Storage) hasTable(ctx context.Context) (bool, error) {
	row := s.conn.QueryRowContext(ctx, `
		SELECT count(*) 
		FROM information_schema.tables 
		WHERE table_schema = $1 AND table_name = $2 AND table_type = $3`,
		s.schema, s.table, "BASE TABLE")

	var count int64
	if err := row.Scan(&count); err != nil {
		return false, fmt.Errorf("row.Scan: %w", err)
	}

	return count > 0, nil
}

func (s *Storage) createTaskTable(ctx context.Context) error {
	createTableQuery := fmt.Sprintf(`
	CREATE TABLE %q.%q (
		"id"         BIGSERIAL,
  		"created_at" TIMESTAMPTZ NOT NULL DEFAULT '1970-01-01',
  		"updated_at" TIMESTAMPTZ NOT NULL DEFAULT '1970-01-01',
		
		"in_work_at" TIMESTAMPTZ,
		"done_at"    TIMESTAMPTZ,
		"error"      TEXT,
		
		"start_after" TIMESTAMPTZ NOT NULL,
		"handler"     TEXT        NOT NULL,
		"data"        JSONB,
		
		PRIMARY KEY ("id")
	)`, s.schema, s.table)

	createIndexInWorkQuery := fmt.Sprintf(`CREATE INDEX "%s_idx_in_work_at" ON %q.%q ("in_work_at");`,
		s.table, s.schema, s.table)

	createIndexStartAfterQuery := fmt.Sprintf(`CREATE INDEX "%s_idx_start_after" ON %q.%q ("start_after");`,
		s.table, s.schema, s.table)

	createIndexHandlerQuery := fmt.Sprintf(`CREATE INDEX "%s_idx_handler" ON %q.%q ("handler");`,
		s.table, s.schema, s.table)

	tx, err := s.conn.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("conn.BeginTx: %w", err)
	}

	if _, err = tx.ExecContext(ctx, createTableQuery); err != nil {
		return rollback(tx, fmt.Errorf("tx.ExecContext createTableQuery: %w", err))
	}

	if _, err = tx.ExecContext(ctx, createIndexInWorkQuery); err != nil {
		return rollback(tx, fmt.Errorf("tx.ExecContext createIndexInWorkQuery: %w", err))
	}

	if _, err = tx.ExecContext(ctx, createIndexStartAfterQuery); err != nil {
		return rollback(tx, fmt.Errorf("tx.ExecContext createIndexStartAfterQuery: %w", err))
	}

	if _, err = tx.ExecContext(ctx, createIndexHandlerQuery); err != nil {
		return rollback(tx, fmt.Errorf("tx.ExecContext createIndexHandlerQuery: %w", err))
	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("tx.Commit: %w", err)
	}

	return nil
}

// GetNextTask returns the oldest task that the worker has not yet taken to work.
// Value of the StartAfter must be less then "now" argument.
// Returns error ErrTaskNotFound when can't found task.
func (s *Storage) GetNextTask(ctx context.Context, now time.Time) (tmanager.Task, error) {
	task := new(Task)
	realNow := time.Now()

	err := s.conn.QueryRowContext(ctx, s.queryGetNextTask, realNow, realNow, now).
		Scan(&task.ID, &task.Error, &task.StartAfter, &task.Handler, &task.Data)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, tmanager.ErrTaskNotFound
		}

		return nil, fmt.Errorf("scan row: %w", err)
	}

	return task, nil
}

func (s *Storage) createTask(ctx context.Context, task tmanager.Task) error {
	now := time.Now()
	_, err := s.conn.ExecContext(
		ctx,
		s.queryCreateTask,
		now, now, task.GetStartAfter(), task.GetHandler(), jsonb(task.GetData()),
	)
	if err != nil {
		return fmt.Errorf("insert row: %w", err)
	}

	return nil
}

// CreateTasks creates list of the tasks in the DB.
func (s *Storage) CreateTasks(ctx context.Context, tasks ...tmanager.Task) error {
	if len(tasks) == 1 {
		return s.createTask(ctx, tasks[0])
	}

	tx, err := s.conn.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("conn.BeginTx: %w", err)
	}

	stmt, err := tx.PrepareContext(ctx, s.queryCreateTask)
	if err != nil {
		return rollback(tx, fmt.Errorf("tx.Prepare stmt: %w", err))
	}

	defer stmt.Close() // nolint:errcheck // without check this error

	now := time.Now()
	for _, task := range tasks {
		_, err = stmt.ExecContext(ctx, now, now, task.GetStartAfter(), task.GetHandler(), jsonb(task.GetData()))
		if err != nil {
			return rollback(tx, fmt.Errorf("insert row: %w", err))
		}
	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("tx.Commit: %w", err)
	}

	return nil
}

// SaveTaskWithSuccess saves success done task.
func (s *Storage) SaveTaskWithSuccess(ctx context.Context, task tmanager.Task) error {
	t, err := convertIfaceTask(task)
	if err != nil {
		return fmt.Errorf("convertIfaceTask: %w", err)
	}

	now := time.Now()

	_, err = s.conn.ExecContext(ctx, s.queryTaskWithSuccess, now, now, t.ID)
	if err != nil {
		return fmt.Errorf("update row: %w", err)
	}

	return nil
}

// SaveTaskWithError saves task with error.
// Saved previous error text in the column.
func (s *Storage) SaveTaskWithError(ctx context.Context, task tmanager.Task, taskErr error) error {
	t, err := convertIfaceTask(task)
	if err != nil {
		return fmt.Errorf("convertIfaceTask: %w", err)
	}

	now := time.Now()

	errText := fmt.Sprintf("[%s] %s", now.Format(time.RFC3339), taskErr)

	_, err = s.conn.ExecContext(ctx, s.queryTaskWithError, now, errText, t.ID)
	if err != nil {
		return fmt.Errorf("update row: %w", err)
	}

	return nil
}

// createUniqueIndex creates index for only one handler task not done.
func (s *Storage) createUniqueIndex(ctx context.Context, taskHandler string) error {
	indexName := fmt.Sprintf("idx_one_active_task_%s", taskHandler)

	if _, ok := s.createdUniqueIndexes[indexName]; ok {
		return nil
	}

	query := fmt.Sprintf(
		`CREATE UNIQUE INDEX IF NOT EXISTS %s ON %q.%q ("handler")
			WHERE "handler" = '%s' AND "done_at" IS NULL;`,
		indexName, s.schema, s.table, taskHandler,
	)

	_, err := s.conn.ExecContext(ctx, query)
	if err != nil {
		return fmt.Errorf("create index: %w", err)
	}

	s.createdUniqueIndexes[indexName] = struct{}{}

	return nil
}

// CreateOneActiveTask creates table constraint to control that DB
// contains only one active task and creates task with ignoring conflict.
func (s *Storage) CreateOneActiveTask(ctx context.Context, task tmanager.Task) error {
	err := s.createUniqueIndex(ctx, task.GetHandler())
	if err != nil {
		return fmt.Errorf("createUniqueIndex: %w", err)
	}

	now := time.Now()
	_, err = s.conn.ExecContext(
		ctx,
		s.queryCreateTaskWithIgnore,
		now, now, task.GetStartAfter(), task.GetHandler(), jsonb(task.GetData()),
	)
	if err != nil {
		return fmt.Errorf("insert task: %w", err)
	}

	return nil
}

// RestartTask makes the task free to re-taking in GetNextTask method.
// You have to set time to re-taking.
func (s *Storage) RestartTask(ctx context.Context, task tmanager.Task, restartAfter time.Time) error {
	t, err := convertIfaceTask(task)
	if err != nil {
		return fmt.Errorf("convertIfaceTask: %w", err)
	}

	now := time.Now()

	_, err = s.conn.ExecContext(ctx, s.queryRestartTask, now, restartAfter, t.ID)
	if err != nil {
		return fmt.Errorf("update row: %w", err)
	}

	return nil
}

func scanInspectorTasks(rows *sql.Rows) ([]inspector.Task, error) {
	tasks := make([]inspector.Task, 0, 10)

	for rows.Next() {
		task := new(Task)
		err := rows.Scan(&task.ID, &task.Error, &task.StartAfter, &task.Handler, &task.Data)
		if err != nil {
			return nil, fmt.Errorf("scan task: %w", err)
		}

		tasks = append(tasks, task)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error in rows: %w", err)
	}

	return tasks, nil
}

// GetStuckedTasks returns stucked tasks.
// Stuck task it is task without error and taked in work before inWorkBefore.
// Returns empty slcie when storage can't found stucked tasks.
func (s *Storage) GetStuckedTasks(ctx context.Context, inWorkBefore time.Time) ([]inspector.Task, error) {
	rows, err := s.conn.QueryContext(ctx, s.queryGetStuckedTasks, inWorkBefore)
	if err != nil {
		return nil, fmt.Errorf("query rows: %w", err)
	}

	defer rows.Close() // nolint:errcheck // don't care about rows.Close error

	tasks, err := scanInspectorTasks(rows)
	if err != nil {
		return nil, fmt.Errorf("scan inspector tasks: %w", err)
	}

	return tasks, nil
}

// GetFailedTasks returns failed tasks.
// Failed task it is undone task with error.
// Returns empty slcie when storage can't found failed tasks.
func (s *Storage) GetFailedTasks(ctx context.Context) ([]inspector.Task, error) {
	rows, err := s.conn.QueryContext(ctx, s.queryGetFailedTasks)
	if err != nil {
		return nil, fmt.Errorf("query rows: %w", err)
	}

	defer rows.Close() // nolint:errcheck // don't care about rows.Close error

	tasks, err := scanInspectorTasks(rows)
	if err != nil {
		return nil, fmt.Errorf("scan inspector tasks: %w", err)
	}

	return tasks, nil
}

// DeleteCompletedTasks delete completed tasks completed before olderThen param.
func (s *Storage) DeleteCompletedTasks(ctx context.Context, olderThen time.Time) error {
	_, err := s.conn.ExecContext(ctx, s.queryDeleteCompletedTasks, olderThen)
	if err != nil {
		return fmt.Errorf("delete tasks: %w", err)
	}

	return nil
}

// DeleteNamedCompletedTasks delete completed tasks with special handler names
// and completed before olderThen param.
func (s *Storage) DeleteNamedCompletedTasks(ctx context.Context, olderThen time.Time, names ...string) error {
	namesArray := &pgtype.TextArray{
		Elements:   make([]pgtype.Text, len(names)),
		Dimensions: []pgtype.ArrayDimension{{Length: int32(len(names)), LowerBound: 1}},
		Status:     pgtype.Present,
	}

	for i := range names {
		namesArray.Elements[i] = pgtype.Text{String: names[i], Status: pgtype.Present}
	}

	_, err := s.conn.ExecContext(ctx, s.queryDeleteNamedCompletedTasks, olderThen, namesArray)
	if err != nil {
		return fmt.Errorf("delete tasks: %w", err)
	}

	return nil
}
