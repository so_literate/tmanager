// Package postgresql implements tmanager.Storage interface using Postgresql database.
package postgresql

import (
	"context"
	"database/sql"
	"fmt"
)

// Storage contains methods to manage tasks in database.
type Storage struct {
	conn *sql.DB

	schema string
	table  string

	queryGetNextTask          string
	queryCreateTask           string
	queryCreateTaskWithIgnore string
	queryTaskWithSuccess      string
	queryTaskWithError        string
	queryRestartTask          string

	queryGetStuckedTasks string
	queryGetFailedTasks  string

	queryDeleteCompletedTasks      string
	queryDeleteNamedCompletedTasks string

	createdUniqueIndexes map[string]struct{}
}

// Config optional config of the postgresql connection.
// Feel free to skip any of fields.
type Config struct {
	// Schema with tables in database, postgres current_schema by default.
	Schema string
	// TableName table name with tasks, "tmanager_tasks" by default.
	TableName string
}

// New creates new schema and task table if not exists.
// Takes std sql connection and optional (may be nil) config with schema and table name.
func New(conn *sql.DB, config *Config) (*Storage, error) {
	if config == nil {
		config = new(Config)
	}

	s := &Storage{
		conn: conn,

		schema: config.Schema,
		table:  config.TableName,

		createdUniqueIndexes: make(map[string]struct{}, 3),
	}

	if err := s.checkAndSetDefaultConfigValues(); err != nil {
		return nil, fmt.Errorf("checkAndSetDefaultConfigValues: %w", err)
	}

	if err := s.init(); err != nil {
		return nil, fmt.Errorf("s.init: %w", err)
	}

	return s, nil
}

func (s *Storage) checkAndSetDefaultConfigValues() error {
	if s.table == "" {
		s.table = "tmanager_tasks"
	}

	if s.schema == "" {
		currentSchema, err := s.getCurrentSchema(context.Background())
		if err != nil {
			return fmt.Errorf("getCurrentSchema: %w", err)
		}

		s.schema = currentSchema
	}

	return nil
}

func (s *Storage) prepareQueryList() {
	s.queryGetNextTask = fmt.Sprintf(`UPDATE %q.%q 
		SET in_work_at = $1, "updated_at" = $2
		WHERE "id" = (
			SELECT "id" FROM %q.%q
				WHERE "in_work_at" IS NULL AND "start_after" < $3
				ORDER BY "created_at" ASC
				LIMIT 1 
				FOR UPDATE SKIP LOCKED
			)
		RETURNING "id", "error", "start_after", "handler", "data"`,
		s.schema, s.table, s.schema, s.table)

	s.queryCreateTask = fmt.Sprintf(`INSERT INTO %q.%q 
		("created_at", "updated_at", "start_after", "handler", "data") 
		VALUES ($1, $2, $3, $4, $5)`, s.schema, s.table)

	s.queryCreateTaskWithIgnore = s.queryCreateTask + ` ON CONFLICT DO NOTHING`

	s.queryTaskWithSuccess = fmt.Sprintf(
		`UPDATE %q.%q SET "updated_at" = $1, "done_at" = $2 WHERE "id" = $3`,
		s.schema, s.table,
	)

	s.queryTaskWithError = fmt.Sprintf(
		`UPDATE %q.%q SET "updated_at" = $1, "error" = $2 WHERE "id" = $3`,
		s.schema, s.table,
	)

	s.queryRestartTask = fmt.Sprintf(
		`UPDATE %q.%q SET "updated_at" = $1, "in_work_at" = NULL, "start_after" = $2 WHERE "id" = $3`,
		s.schema, s.table,
	)

	s.queryGetStuckedTasks = fmt.Sprintf(
		`SELECT "id", "error", "start_after", "handler", "data" FROM %q.%q
			WHERE "done_at" IS NULL AND "error" IS NULL AND "in_work_at" < $1
			ORDER BY "id";`,
		s.schema, s.table,
	)

	s.queryGetFailedTasks = fmt.Sprintf(
		`SELECT "id", "error", "start_after", "handler", "data" FROM %q.%q
			WHERE "done_at" IS NULL AND "error" IS NOT NULL
			ORDER BY "id";`,
		s.schema, s.table,
	)

	s.queryDeleteCompletedTasks = fmt.Sprintf(
		`DELETE FROM %q.%q WHERE "done_at" < $1`,
		s.schema, s.table,
	)

	s.queryDeleteNamedCompletedTasks = fmt.Sprintf(
		`DELETE FROM %q.%q WHERE "done_at" < $1 AND "handler" = ANY ($2)`,
		s.schema, s.table,
	)
}

func (s *Storage) init() error {
	ctx := context.Background()
	hasTable, err := s.hasTable(ctx)
	if err != nil {
		return fmt.Errorf("hasTable (%s.%s): %w", s.schema, s.table, err)
	}

	s.prepareQueryList()

	if hasTable {
		return nil
	}

	if err = s.createTaskTable(ctx); err != nil {
		return fmt.Errorf("createTaskTable: %w", err)
	}

	return nil
}
