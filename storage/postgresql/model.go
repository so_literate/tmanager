package postgresql

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/so_literate/tmanager"
)

// ErrInvalidDataJSONB returns when database returns wrong type for JSON field.
var ErrInvalidDataJSONB = errors.New("invalid data for JSONB type")

type jsonb json.RawMessage

// Value returns driver.Value representation of the json.
// If value is nil, then returns SQL NULL. Doesn't check if JSON is valid.
func (j jsonb) Value() (driver.Value, error) {
	if j == nil {
		return nil, nil
	}

	return string(j), nil
}

// Scan stores the src in *j without json validation.
func (j *jsonb) Scan(src interface{}) error {
	switch t := src.(type) {
	case string:
		*j = append((*j)[0:0], []byte(t)...)

	case []byte:
		*j = append((*j)[0:0], t...)

	case nil:
		*j = nil

	default:
		return fmt.Errorf("%w: %#v", ErrInvalidDataJSONB, t)
	}

	return nil
}

// Task is a database model of the task.
type Task struct {
	ID        int64
	CreatedAt time.Time
	UpdatedAt time.Time

	InWorkAt *time.Time // time when the task was taken to work, will be nil in the new task
	DoneAt   *time.Time // time when the task was done
	Error    *string    // error text of the task

	StartAfter time.Time // time after which the task should be started.
	Handler    string    // name of the task handler
	Data       jsonb     // data of the handler
}

// String returns json representation of the task model.
func (t *Task) String() string {
	res, err := json.Marshal(t)
	if err != nil {
		return "json.Marshal: " + err.Error()
	}

	return string(res)
}

// GetID returns unique identification of the task.
func (t *Task) GetID() string { return strconv.FormatInt(t.ID, 10) }

// GetStartAfter returns time after which the task should be started.
func (t *Task) GetStartAfter() time.Time { return t.StartAfter }

// GetHandler returns name of the task handler.
func (t *Task) GetHandler() string { return t.Handler }

// GetData returns data of the handler.
func (t *Task) GetData() json.RawMessage { return json.RawMessage(t.Data) }

// GetError returns saved error of the task execution.
func (t *Task) GetError() string {
	if t.Error == nil {
		return ""
	}
	return *t.Error
}

func convertIfaceTask(task tmanager.Task) (*Task, error) {
	res, ok := task.(*Task)
	if ok {
		return res, nil
	}

	res = &Task{
		StartAfter: task.GetStartAfter(),
		Handler:    task.GetHandler(),
		Data:       jsonb(task.GetData()),
	}

	id := task.GetID()
	if id == "" || id == "0" {
		return res, nil
	}

	var err error
	res.ID, err = strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("parse string id: %w", err)
	}

	return res, nil
}
